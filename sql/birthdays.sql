# code to get birthday for a child with a given month
select 
display_name,
birth_date,
street_address,
city,
civicrm_state_province.name,
postal_code
FROM civicrm_contact
left join civicrm_address on civicrm_contact.id = civicrm_address.contact_id
left join civicrm_state_province on civicrm_address.state_province_id = civicrm_state_province.id
where contact_sub_type = 'Child'
and extract(MONTH FROM birth_date) = 1
order by last_name;
# and civicrm_contact.birth_date between date_format(now() + interval 1 MONTH, '%Y-%m-%d 00:00:00') AND date_format(now(), '%Y-%m-%d 00:00:00')
# and civicrm_contact.birth_date between date_format(now() + interval 1 MONTH, '%Y-%m-%d 00:00:00') AND date_format(now(), '%Y-%m-%d 00:00:00')
;