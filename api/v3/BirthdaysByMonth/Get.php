<?php

//require_once __DIR__.'/ChromePhp.php';

function civicrm_api3_birthdays_by_month_get($params) {
  //  ChromePhp::log(__DIR__);
    $month = $params['month']; // month for birthday
    $group = $params['group'];
    $contact_sub_type = $params['contact_sub_type']; // filter by contact subtype
    
    // get the name for the given sub-type id
    $result = civicrm_api3('ContactType', 'get', array(
        'sequential' => 1,
        'return' => "name",
        'id' => $contact_sub_type,
    ));
    $sub_type_name = ($result['values'][0]['name']);
    
    // Build query
    
    // Add SELECT
    $query =    "SELECT civicrm_contact.id," .
                "display_name," .
                "birth_date," .
                "street_address," .
                "extract(YEAR FROM birth_date) as birth_year," .
                "city," .
                "civicrm_state_province.name," .
                "postal_code";
    
    // Add FROM
    
    $query .= " ";
    $query .=   "FROM civicrm_contact " .
                "left join civicrm_address on civicrm_contact.id = civicrm_address.contact_id " .
                "left join civicrm_state_province on civicrm_address.state_province_id = civicrm_state_province.id";
    
    if ($group) {
        $query .= " ";
        $query .= "left join civicrm_group_contact on civicrm_contact.id = civicrm_group_contact.contact_id";
    }
    
    // Add WHERE
    
    $query .= " ";
    $query .=   "WHERE extract(MONTH FROM birth_date) = " . $month . " " .
                "AND extract(YEAR FROM birth_date) <> '1911'";
    
    // if a contact subtype is specified set the query
    if ($group) {
        $query .= " ";
        $query .= "AND civicrm_group_contact.group_id = " . $group;
    }
    if ($sub_type_name) {
        $query .= " ";
        $query .= "AND contact_sub_type = '" . $sub_type_name . "'";
    }

    //Add GROUP BY & ORDER BY
    
    $query .= " ";
    $query .=   "GROUP BY display_name " .
                "ORDER BY last_name";

//    $query = "select " .
//            "civicrm_contact.id," .
//            "display_name," .
//            "birth_date," .
//            "street_address," .
//            "extract(YEAR FROM birth_date) as birth_year," .
//            "city," .
//            "civicrm_state_province.name," .
//            "postal_code " .
//            "FROM civicrm_contact " .
//            "left join civicrm_address on civicrm_contact.id = civicrm_address.contact_id " .
//            "left join civicrm_state_province on civicrm_address.state_province_id = civicrm_state_province.id " .
//            $subTypeQuery .
//            " extract(MONTH FROM birth_date) = " . $month .
//            //" ORDER BY last_name" .
//            " and extract(YEAR FROM birth_date) <> '1911' GROUP BY display_name ORDER BY last_name";

    //ChromePhp::log($query);

    $dao = CRM_Core_DAO::executeQuery($query);
    $results = array();
    while ($dao->fetch()) {
        $results[] = $dao->toArray();
    }

    //ChromePhp::log($results);

    return civicrm_api3_create_success($results);
}


// SET PERMISSIONS!
function birthdaysbymonth_civicrm_alterAPIPermissions($entity, $action, &$params, &$permissions) {
        $permissions['birthdays_by_month']['get'] = array('access CiviCRM');
}
